/** create a module for your app **/
var App = angular.module('app', ['ui.router', 'ngCookies']);

/** App configuration **/
App.config(['$stateProvider', '$urlRouterProvider',
            function ($stateProvider, $urlRouterProvider) {

        $.support.cors = true;

        /** configure routes **/
        $urlRouterProvider.otherwise('/list');

        $stateProvider.state('list', {
            url: '/list',
            templateUrl: 'list/list.html',
            controller: 'ListCtrl'
        })
}]);