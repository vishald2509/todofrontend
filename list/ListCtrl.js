/** Controller for List view **/

App.controller('ListCtrl', ['$scope', '$state', function($scope, $state)
{
    
    $scope.list = [
        {task:'Task', status:'true',}
    ];
    $scope.add = function() {
        var objects = $scope.list;
        var flag=0
         for (var i = 0; i < objects.length; i++) {
            if (objects[i].task === $scope.taskname) {
                flag=1;                
                console.log('Task already exists!'+$scope.taskname);
                break;
            }
        }
        
        if(flag==0){
        $scope.list.push({
            task:$scope.taskname, status:'false'
        });
        }
        
        }
    $scope.featureSelect = function(x, event){
          // how to check if checkbox is selected or not
        var objects = $scope.list;
         for (var i = 0; i < objects.length; i++) {
            if (objects[i].task === x) {
                objects[i].status = event.target.checked;
                break;
            }
        }                  
         
        };
    
    
}]);